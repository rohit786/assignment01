package bopselenium;
import java.io.IOException;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class Assignment01 {
	 public void taskkill(String k) throws IOException
	    {
	        
	            if(k.equals("chrome"))
	            {
	                
	            Runtime.getRuntime().exec("TASKKILL /f /im chromedriver.exe");
	            
	            }
	            if(k.equals("firefox"))
	            {
	                Runtime.getRuntime().exec("TASKKILL /f /im geckodriver.exe");
	            }
	            if(k.equals("ie"))
	            {
	                Runtime.getRuntime().exec("TASKKILL /f /im IEDriverServer.exe");
	            }
	            if(k.equals("edge")) 
	            {
	                Runtime.getRuntime().exec("TASKKILL /f /im MicrosoftWebDriver.exe");
	            }
	            
	    }

	 
	public static void main(String[] args) throws IOException, InterruptedException  {
		System.setProperty("webdriver.chrome.driver", "D:\\RO20111746\\bopselenium\\Drivers\\chromedriver.exe");
		WebDriver cd = new ChromeDriver();
		cd.get(" http://www.store.demoqa.com");
		Thread.sleep(3000);
		
		System.setProperty("webdriver.gecko.driver", "D:\\RO20111746\\bopselenium\\Drivers\\geckodriver.exe");
		WebDriver gd = new FirefoxDriver();
		gd.get(" http://www.store.demoqa.com");
		Thread.sleep(3000);
		
		System.setProperty("webdriver.ie.driver", "D:\\RO20111746\\bopselenium\\Drivers\\IEDriverServer.exe");
		WebDriver id = new InternetExplorerDriver();
		id.get(" http://www.store.demoqa.com");
		Thread.sleep(3000);
		
		System.setProperty("webdriver.edge.driver","D:\\\\RO20111746\\\\bopselenium\\\\Drivers\\MicrosoftWebDriver.exe");
		WebDriver ed = new EdgeDriver();
		ed.get(" http://www.store.demoqa.com");
		Thread.sleep(3000);
		
		Assignment01 ob=new Assignment01();
		ob.taskkill("chrome");
        ob.taskkill("firefox");
        ob.taskkill("ie");
        ob.taskkill("edge");
	
	}
}
